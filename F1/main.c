/*--------------------------------------------------------------------
| Trabalho realizado por:
|   Rafael P. Branco - 87698
|   Samuel L. Vicente - 87704
| LEIC-T 
| Grupo #55
| 13-10-2017
---------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "mplib3.h"
#include "matrix2d.h"
#include <pthread.h>
#include <unistd.h>

#define exch(A, B) { DoubleMatrix2D* C = B; B = A; A = C; }

typedef struct task_info{
  int channelID;
  int lines; 
  int columns;
  int total_tasks;
  int iter;
} Task_info;

/*--------------------------------------------------------------------
| Function: slaveFn 
---------------------------------------------------------------------*/
void *slaveFn(void* arg) {

  DoubleMatrix2D *matrix, *matrix_aux;
  Task_info* info = (Task_info*)arg;
  int line, column, iter;
  double value;
  double buffer[info->columns];
  
  if ((matrix = dm2dNew(info->lines, info->columns)) == NULL) {
    fprintf(stderr, "\nErro: Nao foi possivel alocar memoria para a matriz.\n\n");
    exit(1);
  }

  if ((matrix_aux = dm2dNew(info->lines, info->columns)) == NULL) {
    fprintf(stderr, "\nErro: Nao foi possivel alocar memoria para a matriz.\n\n");
    dm2dFree(matrix);
    exit(1);
  }

  // recebe fatia da main task
  for (line = 0; line < info->lines; line++) {
    while (receberMensagem(info->total_tasks, info->channelID, buffer, sizeof(double) * info->columns) == -1)
      fprintf(stderr, "Falha a receber mensagem. A tentar de novo."); 

    dm2dSetLine(matrix, line, buffer);
  }
  
  dm2dCopy(matrix_aux, matrix);


  for (iter = 0; iter < info->iter; iter++) {
    for (line = 1; line < info->lines - 1; line++)
      for (column = 1; column < info->columns - 1; column++) {
        value = ( dm2dGetEntry(matrix, line-1, column) + dm2dGetEntry(matrix, line+1, column) +
       dm2dGetEntry(matrix, line, column-1) + dm2dGetEntry(matrix, line, column+1) ) / 4.0;
       dm2dSetEntry(matrix_aux, line, column, value);
     }
    
    exch(matrix, matrix_aux);
    // tmp = matrix_aux;
    // matrix_aux = matrix;
    // matrix = tmp;

    // a ultima tarefa so entra neste caso
    if (info->channelID - 1 >= 0) {
      while (receberMensagem(info->channelID - 1, info->channelID, buffer, sizeof(double) * info->columns) == -1)
        fprintf(stderr, "Falha a receber mensagem. A tentar de novo."); 
      dm2dSetLine(matrix, 0, buffer);
      while (enviarMensagem(info->channelID, info->channelID - 1, dm2dGetLine(matrix, 1), sizeof(double) * info->columns) == -1)
        fprintf(stderr, "Falha a enviar mensagem. A tentar de novo.");
    }
    // primeira tarefa so entra neste caso
    if (info->channelID + 1 < info->total_tasks) {
      while (enviarMensagem(info->channelID, info->channelID + 1, dm2dGetLine(matrix, info->lines - 2), sizeof(double) * info->columns) == -1)
        fprintf(stderr, "Falha a enviar mensagem. A tentar de novo.");
      while (receberMensagem(info->channelID + 1, info->channelID, buffer, sizeof(double) * info->columns) == -1)
        fprintf(stderr, "Falha a receber mensagem. A tentar de novo.");
      dm2dSetLine(matrix, info->lines - 1, buffer);
    }

  }
  // envia o resultado para a main task
  for (line = 1; line < info->lines-1; line++) {
    while (enviarMensagem(info->channelID, info->total_tasks, dm2dGetLine(matrix, line), sizeof(double) * info->columns) == -1)
      fprintf(stderr, "Falha a enviar mensagem. A tentar de novo.");
  }

  dm2dFree(matrix);
  dm2dFree(matrix_aux);
  pthread_exit(NULL);
}
/*--------------------------------------------------------------------
| Function: simul
| Responsavel por atribuir tarefas.
| Retorna ponteiro para uma matriz(NULL em caso de erro).
---------------------------------------------------------------------*/

DoubleMatrix2D *simul(DoubleMatrix2D *matrix, int numIteracoes, int trab, int csz, int N) {
  
  int task, i, line, final_line;
  int lines_per_task = N/trab;  
  double buffer[N + 2];
  Task_info info_vec[trab];
  pthread_t tid[trab];

  if (inicializarMPlib(csz * sizeof(double) * (N + 2), trab + 1) != 0) {
    fprintf(stderr, "\nErro ao inicializar MPLib\n");
    return NULL;
  }

  for (task = 0, line = 0, final_line = 0; task < trab; task++, line -= 2, final_line -= 2) {
    info_vec[task].lines = lines_per_task + 2;
    info_vec[task].columns = N + 2;
    info_vec[task].channelID = task;
    info_vec[task].iter = numIteracoes;
    info_vec[task].total_tasks = trab;

    if(pthread_create(&tid[task], NULL, slaveFn, &info_vec[task]) != 0) {
      fprintf(stderr, "\nErro ao lancar tarefa\n");
      return NULL;
    }
    printf("Tarefa lancada!\n");
    
    for (final_line += info_vec[task].lines; line < final_line; line++) {
      while (enviarMensagem(trab, task, dm2dGetLine(matrix, line), sizeof(double) * (N + 2)) == -1)
        fprintf(stderr, "Falha a enviar mensagem. A tentar de novo.");
    }
  }
  
  for (i = 0, final_line = 1; i < trab; i++) {
    for (line = 1; line < info_vec[i].lines - 1; line++, final_line++) {
      while (receberMensagem(i, trab, buffer, sizeof(double) * (N + 2)) == -1)
        fprintf(stderr, "Falha a receber mensagem. A tentar de novo.");
      dm2dSetLine(matrix, final_line, buffer);
    }
    if (pthread_join(tid[i], NULL) != 0) {
      fprintf(stderr, "\nErro ao esperar por tarefa\n");
      return NULL;
    }
  }
  
  return matrix;
}

/*--------------------------------------------------------------------
| Function: parse_integer_or_exit
---------------------------------------------------------------------*/

int parse_integer_or_exit(char const *str, char const *name)
{
  int value;
 
  if(sscanf(str, "%d", &value) != 1) {
    fprintf(stderr, "\nErro no argumento \"%s\".\n\n", name);
    exit(1);
  }
  return value;
}

/*--------------------------------------------------------------------
| Function: parse_double_or_exit
---------------------------------------------------------------------*/

double parse_double_or_exit(char const *str, char const *name)
{
  double value;

  if(sscanf(str, "%lf", &value) != 1) {
    fprintf(stderr, "\nErro no argumento \"%s\".\n\n", name);
    exit(1);
  }
  return value;
}


/*--------------------------------------------------------------------
| Function: main
---------------------------------------------------------------------*/

int main (int argc, char** argv) {

  if(argc != 9) {
    fprintf(stderr, "\nNumero invalido de argumentos.\n");
    fprintf(stderr, "Uso: heatSim N tEsq tSup tDir tInf iteracoes trab csz\n\n");
    return 1;
  }

  /* argv[0] = program name */
  int N = parse_integer_or_exit(argv[1], "N");
  double tEsq = parse_double_or_exit(argv[2], "tEsq");
  double tSup = parse_double_or_exit(argv[3], "tSup");
  double tDir = parse_double_or_exit(argv[4], "tDir");
  double tInf = parse_double_or_exit(argv[5], "tInf");
  int iteracoes = parse_integer_or_exit(argv[6], "iteracoes");
  int trab = parse_integer_or_exit(argv[7], "trab" );
  int csz = parse_integer_or_exit(argv[8], "csz");

  DoubleMatrix2D *matrix,  *result;


  fprintf(stderr, "\nArgumentos:\n"
	" N=%d tEsq=%.1f tSup=%.1f tDir=%.1f tInf=%.1f iteracoes=%d trab=%d csz=%d\n\n",
	N, tEsq, tSup, tDir, tInf, iteracoes, trab, csz);

  if(N < 1 || tEsq < 0 || tSup < 0 || tDir < 0 || tInf < 0 || iteracoes < 1 || trab < 0 || csz < 0 || N % trab != 0) {
    fprintf(stderr, "\nErro: Argumentos invalidos.\n"
	" Lembrar que N >= 1, temperaturas >= 0, iteracoes >= 1, trab > 0, mod(N, trab)==0\n\n");
    return 1;
  }


  if ((matrix = dm2dNew(N+2, N+2)) == NULL) {
    fprintf(stderr, "\nErro: Nao foi possivel alocar memoria para a matriz.\n\n");
    return -1;
  }


  int i;
  
  for(i=0; i<N+2; i++)
    dm2dSetLineTo(matrix, i, 0);

  dm2dSetLineTo (matrix, 0, tSup);
  dm2dSetLineTo (matrix, N+1, tInf);
  dm2dSetColumnTo (matrix, 0, tEsq);
  dm2dSetColumnTo (matrix, N+1, tDir);


  result = simul(matrix, iteracoes, trab, csz, N);
  if (result == NULL) {
    fprintf(stderr, "\nErro na simulacao.\n\n");
    return -1;
  }

  dm2dPrint(result);
  libertarMPlib();
  dm2dFree(matrix);

  return 0;
}

