/*
// Projeto SO - exercicio 1, version 03
// Sistemas Operativos, DEI/IST/ULisboa 2017-18
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "matrix2d.h"


/*--------------------------------------------------------------------
| Function: simul
---------------------------------------------------------------------*/

DoubleMatrix2D *simul(DoubleMatrix2D *matrix, DoubleMatrix2D *matrix_aux, int linhas, int colunas, int numIteracoes) {
  DoubleMatrix2D* tmp;
  double value;

  for(int iter = 0; iter < numIteracoes; iter++) {
    for(int i = 1; i < linhas - 1; i++) {
      for(int j = 1; j < colunas - 1; j++) {
        value = (dm2dGetEntry(matrix, i, j - 1) + dm2dGetEntry(matrix, i + 1, j) + dm2dGetEntry(matrix, i, j + 1) + dm2dGetEntry(matrix, i - 1, j)) / 4;
        dm2dSetEntry(matrix_aux, i, j, value);
      }
    }
    tmp = matrix_aux;
    matrix_aux = matrix;
    matrix = tmp;
  }

  return matrix;
}

/*--------------------------------------------------------------------
| Function: parse_integer_or_exit
---------------------------------------------------------------------*/

int parse_integer_or_exit(char const *str, char const *name)
{
  int value;
 
  if(sscanf(str, "%d", &value) != 1 || value < 1) {
    fprintf(stderr, "\nErro no argumento \"%s\".\n\n", name);
    exit(1);
  }
  return value;
}

/*--------------------------------------------------------------------
| Function: parse_double_or_exit
---------------------------------------------------------------------*/

double parse_double_or_exit(char const *str, char const *name)
{
  double value;

  if(sscanf(str, "%lf", &value) != 1 || value < 0) {
    fprintf(stderr, "\nErro no argumento \"%s\".\n\n", name);
    exit(1);
  }
  return value;
}


/*--------------------------------------------------------------------
| Function: main
---------------------------------------------------------------------*/

int main (int argc, char** argv) {

  if(argc != 7) {
    fprintf(stderr, "\nNumero invalido de argumentos.\n");
    fprintf(stderr, "Uso: heatSim N tEsq tSup tDir tInf iteracoes\n\n");
    return 1;
  }

  /* argv[0] = program name */
  int N = parse_integer_or_exit(argv[1], "N");
  double tEsq = parse_double_or_exit(argv[2], "tEsq");
  double tSup = parse_double_or_exit(argv[3], "tSup");
  double tDir = parse_double_or_exit(argv[4], "tDir");
  double tInf = parse_double_or_exit(argv[5], "tInf");
  int iteracoes = parse_integer_or_exit(argv[6], "iteracoes");

  DoubleMatrix2D *matrix, *matrix_aux, *result;


  fprintf(stderr, "\nArgumentos:\n"
	" N=%d tEsq=%.1f tSup=%.1f tDir=%.1f tInf=%.1f iteracoes=%d\n",
	N, tEsq, tSup, tDir, tInf, iteracoes);


  matrix = dm2dNew(N+2, N+2);
  matrix_aux = dm2dNew(N+2, N+2);


  /* FAZER ALTERACOES AQUI */
  
/*  if ( N < 1 || iteracoes < 1 || tEsq < 0 || tSup < 0 || tDir < 0 || tInf < 0 )
    return 1;*/

  for (int i = 0; i < N+2; i++) {
    dm2dSetLineTo(matrix, i, i);
  }
  dm2dPrint(matrix);

  for (int i = 0; i < N+2; i++) {
    dm2dSetLine(matrix_aux, i, dm2dGetLine(matrix, i));
  }
  dm2dPrint(matrix_aux);

  dm2dSetLine(matrix_aux, 2, dm2dGetLine(matrix, 3));
  dm2dPrint(matrix_aux);

  dm2dSetLineTo (matrix, 0, tSup);
  dm2dSetLineTo (matrix, N+1, tInf);
  dm2dSetColumnTo (matrix, 0, tEsq);
  dm2dSetColumnTo (matrix, N+1, tDir);

  dm2dCopy (matrix_aux, matrix);

  result = simul(matrix, matrix_aux, N+2, N+2, iteracoes);

  dm2dPrint(result);

  dm2dFree(matrix);
  dm2dFree(matrix_aux);

  return 0;
}
