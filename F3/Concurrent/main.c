/*
// Projeto SO - exercicio 3, version 03
// Sistemas Operativos, DEI/IST/ULisboa 2017-18
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include "matrix2d.h"
#include "barrier.h"

#define excgMatrix(A, B) {DoubleMatrix2D *tmp; tmp = A; A = B; B = tmp;}

typedef struct global_info {
  int N, iter, tam_fatia;
  double maxD;
  DoubleMatrix2D *matrix, *matrix_aux;
} Global_info;

typedef struct thread_info {
  int id;
  Global_info* ginfo;
} Thread_info;


/*--------------------------------------------------------------------
  | Function: tarefa_trabalhadora
  ---------------------------------------------------------------------*/

void *tarefa_trabalhadora(void *args) {
  // Declaracao Variaveis
  int N, iteracoes, tam_fatia, id, iter, i, j, linha_final, linha_inicial, colunas, dyn_stop;
  double value, auxD, maxD, dif;
  DoubleMatrix2D *m, *aux;
  Thread_info *tinfo = (Thread_info *)args;
  Global_info *ginfo = tinfo->ginfo;
  bool vote;

  //Inicializacao Variaveis
  id = tinfo->id;
  N = ginfo->N;
  iteracoes = ginfo->iter;
  tam_fatia = ginfo->tam_fatia;
  maxD = ginfo->maxD;

  m = ginfo->matrix;
  aux = ginfo->matrix_aux;
  colunas = N+2;

  // Linhas
  linha_final = id * tam_fatia + tam_fatia + 1;
  linha_inicial = id * tam_fatia + 1;

  for (iter = 0; iter < iteracoes; iter++) {
    dif = 0;
    vote = false;
    for (i = linha_inicial; i < linha_final; i++)
      for (j = 1; j < colunas - 1; j++) {
        // Calcula Novo Valor para (m, i, j)
        value = ( dm2dGetEntry(m, i-1, j) + dm2dGetEntry(m, i+1, j) +
            dm2dGetEntry(m, i, j-1) + dm2dGetEntry(m, i, j+1) ) / 4.0;

        // Atualiza Diferenca
        auxD = value - dm2dGetEntry(m, i, j);
        if (auxD > dif) dif = auxD;

        // Substitui Escreve Valor
        dm2dSetEntry(aux, i, j, value);
      }

    // Atualiza vote
    if (dif <= maxD)
      vote = true;

    excgMatrix(aux, m);

    // Espera pelas restantes threads
    dyn_stop = waitBarrier(vote);

    if (dyn_stop == 1)
      return m;

    if (dyn_stop != 0) {
      fprintf(stderr, "\nErro ao esperar na barreira.\n");
      exit(-1);
    }
  }
  return m;
}

/*--------------------------------------------------------------------
  | Function: parse_integer_or_exit
  ---------------------------------------------------------------------*/

int parse_integer_or_exit(char const *str, char const *name)
{
  int value;

  if(sscanf(str, "%d", &value) != 1) {
    fprintf(stderr, "\nErro no argumento \"%s\".\n\n", name);
    exit(1);
  }
  return value;
}

/*--------------------------------------------------------------------
  | Function: parse_double_or_exit
  ---------------------------------------------------------------------*/

double parse_double_or_exit(char const *str, char const *name)
{
  double value;

  if(sscanf(str, "%lf", &value) != 1) {
    fprintf(stderr, "\nErro no argumento \"%s\".\n\n", name);
    exit(1);
  }
  return value;
}


/*--------------------------------------------------------------------
  | Function: main
  ---------------------------------------------------------------------*/

int main (int argc, char** argv) {
  int i;
  pthread_t* trabalhadoras;
  Global_info* ginfo;
  Thread_info* tinfo;
  DoubleMatrix2D **res;

  if(argc != 9) {
    fprintf(stderr, "\nNumero invalido de argumentos.\n");
    fprintf(stderr, "Uso: heatSim N tEsq tSup tDir tInf iteracoes trabalhadoras limiar_paragem\n\n");
    return 1;
  }

  // Inicializa Tarefas
  ginfo = (Global_info*)malloc(sizeof(Global_info));
  if (ginfo == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para ginfo");
    return -1;
  }


  /* argv[0] = program name */
  ginfo->N = parse_integer_or_exit(argv[1], "N");
  double tEsq = parse_double_or_exit(argv[2], "tEsq");
  double tSup = parse_double_or_exit(argv[3], "tSup");
  double tDir = parse_double_or_exit(argv[4], "tDir");
  double tInf = parse_double_or_exit(argv[5], "tInf");
  ginfo->iter = parse_integer_or_exit(argv[6], "iteracoes");
  int trab = parse_integer_or_exit(argv[7], "trab");
  ginfo->maxD = parse_double_or_exit(argv[8], "maxD");

  fprintf(stderr, "\nArgumentos:\n"
      " N=%d tEsq=%.1f tSup=%.1f tDir=%.1f tInf=%.1f iteracoes=%d trabalhadoras=%d limirar de paragem=%.1f\n",
      ginfo->N, tEsq, tSup, tDir, tInf, ginfo->iter, trab, ginfo->maxD);

  // Verifica Input
  if(ginfo->N < 1 || tEsq < 0 || tSup < 0 || tDir < 0 || tInf < 0 || ginfo->iter < 1 || trab < 1 || ginfo->maxD < 0) {
    fprintf(stderr, "\nErro: Argumentos invalidos.\n"
        " Lembrar que N >= 1, temperaturas >= 0, iteracoes >= 1, trabalhadoras > 0 e limiar de paragem >= 0\n\n");
    return 1;
  }

  if (ginfo->N % trab != 0) {
    fprintf(stderr, "\nErro: Argumentos N e trabalhadoras invalidos\n"
        "N deve ser multiplo de trabalhadoras.");
    return -1;
  }

  // Calcula Tamanho da Fatia
  ginfo->tam_fatia = ginfo->N / trab;

  tinfo = (Thread_info*)malloc(trab * sizeof(Thread_info));
  if (tinfo == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para tinfo");
    return -1;
  }

  trabalhadoras = (pthread_t*)malloc(trab * sizeof(pthread_t));
  if (trabalhadoras == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para trabalhadoras");
    return -1;
  }

  res = (DoubleMatrix2D **)malloc(trab * sizeof(DoubleMatrix2D *));
  if (res == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para res");
    return -1;
  }

  // Cria e Inicializa Matrizes
  ginfo->matrix = dm2dNew(ginfo->N+2, ginfo->N+2);
  ginfo->matrix_aux = dm2dNew(ginfo->N+2, ginfo->N+2);

  if (ginfo->matrix == NULL || ginfo->matrix_aux == NULL) {
    fprintf(stderr, "\nErro: Nao foi possivel alocar memoria para as matrizes.\n\n");
    return -1;
  }

  dm2dSetLineTo (ginfo->matrix, 0, tSup);
  dm2dSetLineTo (ginfo->matrix, ginfo->N+1, tInf);
  dm2dSetColumnTo (ginfo->matrix, 0, tEsq);
  dm2dSetColumnTo (ginfo->matrix, ginfo->N+1, tDir);

  dm2dCopy (ginfo->matrix_aux, ginfo->matrix);

  if (initBarrier(trab) != 0) { 
    fprintf(stderr, "\nErro ao criar barreira.\n");
    return -1;
  }

  for (i = 0; i < trab; i++) {
    tinfo[i].id = i;
    tinfo[i].ginfo = ginfo;
    if (pthread_create(&trabalhadoras[i], NULL, tarefa_trabalhadora, &tinfo[i]) != 0) {
      fprintf(stderr, "\nErro ao criar tarefas.\n");
      return -1;
    }
  }

  for (i = 0; i < trab; i++) {
    if (pthread_join(trabalhadoras[i], (void **)&res[i]) != 0) {
      fprintf(stderr, "\nErro ao terminar tarefas.\n");
      return -1;
    }

    // Verifica se todas as tarefas retornaram o mesmo ponteiro
    if (i > 0 && res[0] != res[i]) {
      fprintf(stderr, "\nErro ao calcular.\n");
      return -1;
    }
  }

  dm2dPrint(res[0]);

  if (destroyBarrier() != 0) { 
    fprintf(stderr, "\nErro ao destruir barreira.\n");
    return -1;
  }

  free(tinfo);
  free(trabalhadoras);
  free(res);
  dm2dFree(ginfo->matrix);
  dm2dFree(ginfo->matrix_aux);
  free(ginfo);

  return 0;
}
