#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include "barrier.h"

typedef struct barrier {
  pthread_cond_t wait_for_threads;
  pthread_mutex_t mutex;
  int num_threads;
  // 0 e 1 sao contadores de threads 
  int counter[2];
  // seleciona qual o counter a usar
  int selector;
  // garda o resultada do voto das threads
  bool vote[2];
} Barrier;

Barrier *barrier;

int initBarrier(int num_threads) {
  barrier = (Barrier *)malloc(sizeof(Barrier));
  if (barrier == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para barrier\n");
    return -1;
  }

  if (pthread_cond_init(&barrier->wait_for_threads, NULL) != 0) {
    fprintf(stderr, "\nErro ao inicializar variavel de condicao\n");
    return -1;
  }

  if (pthread_mutex_init(&barrier->mutex, NULL) != 0) {
    fprintf(stderr, "\nErro ao inicializar mutex\n");
    return -1;
  }

  barrier->num_threads = num_threads;

  for (int i = 0; i < 2; i++) {
    barrier->counter[i] = num_threads;
    barrier->vote[i] = true;
  }
  barrier->selector = 0;

  return 0;
}

int waitBarrier(bool vote) {
  // Bloqueia o trinco
  if (pthread_mutex_lock(&barrier->mutex) != 0) {
    fprintf(stderr, "\nErro ao bloquear mutex\n");
    return -1;
  }

  // Qual o Contador a Usar
  int curr_counter = barrier->selector;

  // calcula o resultado do vote
  barrier->vote[curr_counter] &= vote;

  // Atualiza o Contador da Barreira
  if (--barrier->counter[curr_counter] == 0) {
    // Reset do Proximo Contador
    barrier->selector = (curr_counter + 1) % 2;
    barrier->counter[barrier->selector] = barrier->num_threads;

    // Se o voto da barreira atual nao passar, da reset ao da barreira seguinte
    if (!barrier->vote[curr_counter])
      barrier->vote[barrier->selector] = true;

    // Desbloqueia o trinco
    if (pthread_mutex_unlock(&barrier->mutex) != 0) {
      fprintf(stderr, "\nErro ao desbloquear mutex\n");
      return -1;
    }

    // Broadcast
    if (pthread_cond_broadcast(&barrier->wait_for_threads) != 0) {
      fprintf(stderr, "\nErro no broadcast\n");
      return -1;
    }

  } else {
    while (barrier->counter[curr_counter] > 0) {
      // Espera pelas restantes tarefas
      if (pthread_cond_wait(&barrier->wait_for_threads, &barrier->mutex) != 0) {
        fprintf(stderr, "\nErro ao esperar\n");
        return -1;
      }
    }
    // Desbloqueia o trinco
    if (pthread_mutex_unlock(&barrier->mutex) != 0) {
      fprintf(stderr, "\nErro ao desbloquear mutex\n");
      return -1;
    }
  }
  // Verifica o voto
  if (barrier->vote[curr_counter]) 
    return 1;
  return 0;
}

int destroyBarrier() {
  if(pthread_mutex_destroy(&barrier->mutex) != 0) {
    fprintf(stderr, "\nErro ao destruir mutex da barreira\n");
    return -1; 
  }

  if(pthread_cond_destroy(&barrier->wait_for_threads) != 0) {
    fprintf(stderr, "\nErro ao destruir variável de condição\n");
    return -1; 
  }

  free(barrier);
  return 0;
}
