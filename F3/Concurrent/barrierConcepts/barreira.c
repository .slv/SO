#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "barreira.h"


typedef struct barrier {
  pthread_cond_t wait_for_threads;
  pthread_mutex_t mutex;
  unsigned int numThread;
  unsigned int counter;
  bool sense;
} Barrier;

/*--------------------------------------------------------------------
| Global Variables
---------------------------------------------------------------------*/

Barrier *barrier;


int initBarrier(unsigned int numThread) {
  barrier = (Barrier *)malloc(sizeof(Barrier));
  if (barrier == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para barrier\n");
    return -1;
  }

  if (pthread_cond_init(&barrier->wait_for_threads, NULL) != 0) {
    fprintf(stderr, "\nErro ao inicializar variavel de condicao\n");
    return -1;
  }
  
  if (pthread_mutex_init(&barrier->mutex, NULL) != 0) {
    fprintf(stderr, "\nErro ao inicializar mutex\n");
    return -1;
  }

  barrier->numThread = numThread;
  barrier->counter = numThread;
  
  return 0;
}

int waitBarrier(bool *threadSense) {
  *threadSense = !(*threadSense);

  if (pthread_mutex_lock(&barrier->mutex) != 0) {
    fprintf(stderr, "\nErro ao bloquear mutex\n");
    return -1;
  }
  //se estiver na ultima tarefa
  if ((--(barrier->counter)) == 0){
    if (pthread_mutex_unlock(&barrier->mutex) != 0) {
      fprintf(stderr, "\nErro ao desbloquear mutex\n");
      return -1;
    }
    barrier->counter = barrier->numThread;
    barrier->sense = *threadSense;
    if (pthread_cond_broadcast(&barrier->wait_for_threads) != 0) {
      fprintf(stderr, "\nErro ao bloquear mutex\n");
      return -1;
    }
    return 0;
  } 

  else {
    while(barrier->sense != *threadSense)
      if (pthread_cond_wait(&barrier->wait_for_threads, &barrier->mutex) != 0) {
        fprintf(stderr, "\nErro ao bloquear mutex\n");
        return -1;
      }
    if (pthread_mutex_unlock(&barrier->mutex) != 0) {
      fprintf(stderr, "\nErro ao desbloquear mutex\n");
      return -1;
    }
    return 0;

  }
  return -1;
}

int destroyBarrier() {
  
  if(pthread_mutex_destroy(&barrier->mutex) != 0) {
    fprintf(stderr, "\nErro ao destruir mutex da barreira\n");
    return -1; 
  }

  if(pthread_cond_destroy(&barrier->wait_for_threads) != 0) {
    fprintf(stderr, "\nErro ao destruir variável de condição\n");
    return -1; 
  }
  free(barrier);
  return 0;
}
