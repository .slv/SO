#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "barrier2.h"

typedef struct barrier {
  pthread_cond_t wait_for_threads;
  pthread_mutex_t mutex;
  int num_thread;
  int counter;
  bool need_reset;
} Barrier;

Barrier *barrier;

int initBarrier(int num_thread) {
  barrier = (Barrier *)malloc(sizeof(Barrier));
  if (barrier == NULL) {
    fprintf(stderr, "\nErro ao alocar memoria para barrier\n");
    return -1;
  }

  if (pthread_cond_init(&barrier->wait_for_threads, NULL) != 0) {
    fprintf(stderr, "\nErro ao inicializar variavel de condicao\n");
    return -1;
  }

  if (pthread_mutex_init(&barrier->mutex, NULL) != 0) {
    fprintf(stderr, "\nErro ao inicializar mutex\n");
    return -1;
  }

  barrier->num_thread = num_thread;
  barrier->counter = num_thread;
  barrier->need_reset = false;

  return 0;
}

int waitBarrier() {
  if (pthread_mutex_lock(&barrier->mutex) != 0) {
    fprintf(stderr, "\nErro ao desbloquear mutex\n");
    return -1;
  }
  puts("locked");
  if (barrier->need_reset) {
    barrier->counter = barrier->num_thread;
    barrier->need_reset = false;
    puts("reseted");
  }

  if (--barrier->counter == 0) {
    barrier->need_reset = true;
    puts("set need_reset true");
    if (pthread_mutex_unlock(&barrier->mutex) != 0) {
      fprintf(stderr, "\nErro ao bloquear mutex\n");
      return -1;
    }
    puts("unlocked");   
    if (pthread_cond_broadcast(&barrier->wait_for_threads) != 0) {
      fprintf(stderr, "\nErro no broadcast\n");
      return -1;
    }
    puts("broadcasted");

  } else {
    while (barrier->counter > 0) {
      puts("esperando");
      if (pthread_cond_wait(&barrier->wait_for_threads, &barrier->mutex) != 0) {
        fprintf(stderr, "\nErro ao esperar\n");
        return -1;
      }
      puts("sai da espera a recheackar");
    }
    puts("sai mesmo");
    barrier->need_reset = true;
    if (pthread_mutex_unlock(&barrier->mutex) != 0) {
      fprintf(stderr, "\nErro ao desbloquear mutex\n");
      return -1;
    }
    puts("mutex libertada");
  }
  return 0;
}

int destroyBarrier() {
  if(pthread_mutex_destroy(&barrier->mutex) != 0) {
    fprintf(stderr, "\nErro ao destruir mutex da barreira\n");
    return -1; 
  }

  if(pthread_cond_destroy(&barrier->wait_for_threads) != 0) {
    fprintf(stderr, "\nErro ao destruir variável de condição\n");
    return -1; 
  }

  free(barrier);
  return 0;
}
