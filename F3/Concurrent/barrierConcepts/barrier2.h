
/*
// Biblioteca de barreira de Sense Reversal
// Sistemas Operativos, Samuel Vicente e Rafael Branco 2017-18
*/

#ifndef BARRIER_H
#define BARRIER_H

typedef enum {false, true} bool;
int initBarrier(int numThread); //return 0 if success 
int waitBarrier(); // return 0 if success
int destroyBarrier(); // return 0 if success

#endif
