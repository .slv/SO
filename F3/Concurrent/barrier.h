
/*
// Biblioteca de barreira
// Sistemas Operativos, Samuel Vicente e Rafael Branco 2017-18
*/

#ifndef BARRIER_H
#define BARRIER_H

/*--------------------------------------------------------------------
| Function: initBarrier
| Description: Inicializa a barrier com dois contadores, uma mutex,
|              uma variavel de condicao, uma variavel com o index do
|              contador a usar, uma variavel com o numero
|              de tarefas e uma variavel boleana a true.
|              Em caso de sucesso devolve 0.
|              Em caso de erro devolve -1.
|
| Function: destroyBarrier
| Description: Liberta a memória alocada pela Barrier.
|              Em caso de sucesso devolve 0.
|              Em caso de erro devolve -1.
|
| Function: waitBarrier
| Description: Responsavel por sincronizar as tarefas e decidir quando
|              estas devem de parar o seu trabalho.
|              Caso as tarefas devam terminar o seu trabalho retorna 1.
|              Caso as tarefas devam continuar o seu trabalho retorna 0.
|              Em caso de erro devolve -1.
|
--------------------------------------------------------------------*/

int initBarrier(int); 
int destroyBarrier(); 
int waitBarrier(bool);

#endif
